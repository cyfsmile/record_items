/*
    version:1.0.0
    date:2019-10-25
    
*/
// const config = require("../src/config").main();
// const mongoose = require("mongoose");
// const ze = require("../utils/zerox");
// const tip = require("../utils/tip");
// const Router = require('koa-router')();
const colors = require("colors");
const path = require("path");
// const bodyParser = require('koa-bodyparser')
const koaBody = require('koa-body');
const static = require("koa-static");
// const service_module = new(require("./service.module"))();
// const token = require("../src/logic/token/zero.box.jwt");
//启动实例
let main = async koaApp => { //async
    // global.zerox = {
    //     db: ze.db
    // };
    /*
    连接数据库
    */
    // await onDB().catch(() => {
    //     console.log("!!!!写入数据库连接失败!!!!".red);
    // });
    // await outDB("write").catch(() => { //read --> write
    //     console.log("!!!!查询数据库连接失败!!!!".red);
    // });

    /*
        每10分钟做一次服务状态更新
    */
    // ze.wx.schedule({
    //     type: "minute",
    //     time: [0, 10, 20, 30, 40, 50]
    // }, async() => {
    //     await service_module.init()
    // });
    koaApp.use(async(ctx, next) => {
        ctx.set('Access-Control-Allow-Origin', '*');
        ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild,ze_tk');
        ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
        if (ctx.method == 'OPTIONS') {
            // console.log(123)
            ctx.body = 200;
        } else {
            await next();
        }
    });
    koaApp.use(koaBody({
        multipart: true, // 支持文件上传
        // encoding: 'gzip',            //默认utf-8
        multipart: 'true', // 是否支持 multipart-formdate 的表单
        patchNode: true, // 将请求体打到原生 node.js 的ctx.req中
        formLimit: '10mb', //限制表单请求体的大小   单位kb mb
        textLimit: '10mb', //限制 text body 的大小 单位kb mb
        // formidable: {
        //     // maxFields:1000,               //限制字段的数量 默认1000
        //     uploadDir: path.join(__dirname, './upload-files'), // 设置文件上传目录
        //     keepExtensions: true, // 保持文件的后缀
        //     maxFieldsSize: 2 * 1024 * 1024, // 文件上传大小
        //     multipart: true, //是否支持多文件上传
        //     onFileBegin: (name, file) => { // 文件上传前的设置
        //         // console.log('name: ${name}');
        //         // console.log(file);     
        //     },
        // }
    }));
    /*
        设置静态访问地址
    */
    koaApp.use(static(path.join(__dirname, "../public")));
    /*
        token拦截中间件
    */
    // koaApp.use(async(ctx, next) => {
    // console.log(ctx.request.query)
    // let { token_secret } = ctx.request.headers;
    // console.log(token_secret)
    // if (token_secret == config.token_secret) {
    //     await next();
    // } else {
    //     if (config.tokenFilter.indexOf(ctx.request.query.api) > -1) {
    //         await next()
    //     } else {
    //         await token.middleware(ctx, next)
    //     }
    // }
    // if (config.tokenFilter.indexOf(ctx.request.query.api) > -1) {
    //     await next()
    // } else {
    //     await token.middleware(ctx, next)
    // }
    // });
    /*
        设置全局路由
    */
    // const router = require('../src/routes');
    // router(koaApp);
    let port = 7089;
    // config.examples.forEach(example => {
    koaApp.listen(port, () => {
        console.log(("启动服务，监听端口：" + port).toString().green)
    });
    // });
    /*
        初始化模块服务
    */
    // await service_module.init();
    // await service_module.system_init();
}




// let onDB = async() => { //resolve
//     let dbpath = dbPath(config);
//     // console.log(dbpath)
//     let options = dbOptions(dbpath.dbinfo)
//     global.zerox.db.write = mongoose.createConnection(dbpath.path, options);
//     return await global.zerox.db.write.then((result) => {
//         console.log("连接数据库成功@")
//         return;
//     }).catch((err) => {
//         throw "写入数据库-连接失败！"
//     })
// }
// let outDB = async(type) => { //resolve
//     let dbpath = dbPath(config, type);
//     // console.log(dbpath)
//     let options = dbOptions(dbpath.dbinfo)
//     global.zerox.db.read = mongoose.createConnection(dbpath.path, options);
//     return await global.zerox.db.read.then((result) => {
//         console.log("连接数据库成功@" + type)
//         return;
//     }).catch((err) => {
//         throw "查询数据库-连接失败！"
//     });
// }

let center = async(koaApp) => {
        // koaApp.use((ctx, next) => {
        //     console.log(ctx.request.url);
        //     next()
        // });
        // koaApp.use((ctx, next) => {
        //     console.log(3)
        //     next()
        // });
        return true
    }
    // let dbPath = (config, type) => {
    //     type != 'read' ? type = 'write' : false;
    //     return {
    //         path: config.db.name + "://" + config.db[type].host + ":" + config.db[type].port + "/" + config.db.source,
    //         dbinfo: config.db[type]
    //     }
    // }
    // let dbOptions = (dbinfo) => {
    //     let options = {
    //         // useMongoClient: true,
    //         // socketTimeoutMS: 30000,
    //         keepAlive: 30000,
    //         reconnectTries: 30000,
    //         useNewUrlParser: true, //强制使用新解析器  必填！
    //         useUnifiedTopology: true,
    //         // useCreateIndex: true, //使用mongoose方式创建索引
    //         useCreateIndex: true,
    //         useFindAndModify: false, //使用本地做一些运算
    //         // useNewUrlParser: true
    //         // autoIndex: false, //去除mongo的默认索引
    //         poolSize: 10, //增加连接数
    //         // bufferMaxEntries: 0, //与数据库断开连接马上停止数据操作  防止数据错乱
    //         // connectTimeoutMS: 5000, //连接到数据库失败后的等待时间
    //         socketTimeoutMS: 45000, //一个连接自动杀死前保持的最长连接时间 默认30000
    //         family: 4 // 是使用IPv4还是IPv6进行连接。此选项传递给Node.js的dns.lookup()函数。如果未指定此选项，则MongoDB驱动程序将首先尝试IPv6，然后在IPv6失败时尝试IPv4。如果您的mongoose.connect(uri)通话需要很长时间，请尝试mongoose.connect(uri, { family: 4 })
    //     }
    //     if (dbinfo.loginType == "user") {
    //         options.user = dbinfo.user;
    //         options.pass = dbinfo.pass;
    //     }
    //     return options;
    // }
module.exports = {
    main: main
}