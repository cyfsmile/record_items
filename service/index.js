/**
 * 
 * 服务主文件
 * 
 */
// const express = require("express");
const Koa = require("koa");
const path = require("path")
const koaApp = new Koa();
const ServiceMain = require("./main");
//设置静态文件夹
// ExpressFun.use(express.static(path.join(__dirname, '../public')));
//启动服务
ServiceMain.main(koaApp);